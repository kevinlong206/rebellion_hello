package main

import (
	"testing"
)

func TestEnglish(t *testing.T) {
	t.Run("Some test", func(t *testing.T) {
		lang_shortcode, err := determine_lang("This is some boilerplate text.")
		println("Language shortcode is " + lang_shortcode)
		if err != nil {
			t.Fatalf("Test did not pass: %s", err.Error())
		}
		if lang_shortcode != "eng" {
			t.Fatalf("English language was not detected when it should have been")
		}

	})
}
