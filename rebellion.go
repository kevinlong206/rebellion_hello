package main

import (
	"errors"
	"fmt"
	"os"

	"github.com/abadojack/whatlanggo"
	"github.com/tcnksm/go-input"
)

func determine_lang(input_text string) (string, error) {
	detected_lang := whatlanggo.Detect(input_text).Lang.Iso6393() // this call does not return an "error" type, it just returns an empty string if the language cannot be detected.
	if detected_lang == "" {
		return "", errors.New("sorry, no language could be detected from the input text")
	} else {
		return detected_lang, nil
	}
}

func main() {
	hellos := map[string]string{
		"ara": "Ahlan",
		"ces": "Ahoj",
		"zho": "Nǐn hǎo",
		"fra": "Bonjour",
		"spa": "Hola",
		"rus": "Privet",
		"ita": "Ciao",
		"jpn": "Konnichiwa",
		"deu": "Guten Tag",
		"por": "Olá",
		"kor": "Anyoung",
		"dan": "Goddag",
		"swa": "Hujambo",
		"nld": "Goedendag",
		"ell": "Yassou",
		"eng": "Hello",
	}
	ui := &input.UI{
		Writer: os.Stdout,
		Reader: os.Stdin,
	}

	// Prompt the user to enter arbitrary text via stdin
	query := "==> "
	input_text, err := ui.Ask(query, &input.Options{
		HideDefault: true,
		Required:    true,
		Loop:        true,
		HideOrder:   true,
	})
	// Bail if input was unsuccesful, such as if the user types ctrl+c
	if err != nil {
		panic(err)
	}

	// Attempt to determine language from the inputted text, this function call was separated out for the purpose of unit testing.
	lang_shortcode, err := determine_lang(input_text)

	if err != nil {
		panic(err)
	}

	// Look up the appropriate greeting in the map and print
	// Interesting observation, if the key does not exist in the map(), go does not actually throw an error,
	// for example, I typed "por favor" and the detected language was "epo", (for esperanto), which is detectected
	// by whatlanggo, but does not exist in our map, so the program just printed " World!"

	if hellos[lang_shortcode] != "" {
		fmt.Println(hellos[lang_shortcode] + " World!")
	} else {
		panicmsg := fmt.Sprintf("The language %s was detected, but sadly we do not know the appropriate greeting for that language.", lang_shortcode)
		panic(panicmsg)
	}

}
